import pytest
import homework

@pytest.mark.parametrize("li, indices", [([1, 2, 3, 4], 5)])
def test_take_from_list_wrong_index(li, indices):
    with pytest.raises(IndexError, match=f"Index {indices} is to big for list of length {len(li)}"):
        homework.take_from_list(li, indices)

@pytest.mark.parametrize("li, indices", [([1, 2, 3, 4], 3)])
def test_take_from_list_int(li, indices):
    assert homework.take_from_list(li, indices) == [4]

@pytest.mark.parametrize("in_file,out_file", [('test.txt', 'output.json')])
def test_calc_no_file(in_file, out_file):
    with pytest.raises(FileNotFoundError, match= "No such file or directory: 'test.txt'"):
        homework.calculate(in_file, out_file)
